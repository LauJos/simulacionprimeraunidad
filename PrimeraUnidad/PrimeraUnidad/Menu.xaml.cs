﻿using System;
using Microsoft.Win32;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using PrimeraUnidad.Tools;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PrimeraUnidad.Metodos;

namespace PrimeraUnidad
{
    /// <summary>
    /// Lógica de interacción para Menu.xaml
    /// </summary>
    public partial class Menu : Window
    {
        double x = 0;
        int k;
        int g;
        double c;
        int a;
        int m;
        int num;
        int cont = 0;
        double r;
        List<TablaCongruencialLineal> tablaCon;
        Exportar exportar;
        Random re = new Random();
        MetodoMultiplicacionConstante metodo;
        public Menu()
        {
            InitializeComponent();
            PonerEnEdicionAutomatico(false);
            PonerEnEdicionManual(false);
            LimpiarCajas();
            metodo = new MetodoMultiplicacionConstante();
        }
        #region botones Congruencial Lineal
        private void PonerEnEdicionAutomatico(bool v)
        {
            txtxi.IsEnabled = v;
            txt.IsEnabled = v;
            txtxiMod.IsEnabled = v;
            txtCantidad.IsEnabled = v;
            txtM.IsEnabled = v;
            txtXo.IsEnabled = v;
            txtMuneroManual.IsEnabled = !v;
        }
        private void PonerEnEdicionManual(bool v)
        {
            txtxi.IsEnabled = v;
            txt.IsEnabled = v;
            txtxiMod.IsEnabled = v;
            txtCantidad.IsEnabled = v;
            txtM.IsEnabled = v;
            txtXo.IsEnabled = v;
            txtMuneroManual.IsEnabled = v;
        }
        private void LimpiarCajas()
        {
            txtxi.Clear();
            txt.Clear();
            txtxiMod.Clear();
            txtCantidad.Clear();
            txtM.Clear();
            txtXo.Clear();
        }

        public bool VerificarSiEsNumero(string valor)
        {
            foreach (var item in valor)
            {
                if (!(char.IsNumber(item)))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }
        private void BtnGraficar_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                LinearAxis x = new LinearAxis()
                {
                    Minimum = double.Parse(tablaCon.Min(p => p.Posicion).ToString()),
                    Maximum = double.Parse(tablaCon.Max(p => p.Posicion).ToString()),
                    Position = AxisPosition.Bottom,
                };
                LinearAxis y = new LinearAxis()
                {

                    Minimum = double.Parse(tablaCon.Min(p => p.U)),
                    Maximum = double.Parse(tablaCon.Max(p => p.U)),
                    Position = AxisPosition.Left,
                };

                PlotModel model = new PlotModel();
                model.Axes.Add(x);
                model.Axes.Add(y);
                model.Title = "Grafica";



                LineSeries linea = new LineSeries();
                //  linea.Color = OxyColor.FromRgb(byte.Parse(re.Next(0, 255).ToString()), byte.Parse(re.Next(0, 255).ToString()), byte.Parse(re.Next(0, 255).ToString()));
                // model.Series.Add(linea);
                //
                foreach (var item in tablaCon)
                {
                    linea.Points.Add(new DataPoint(item.Posicion, double.Parse(item.U)));
                    linea.LineStyle = LineStyle.None;
                    linea.MarkerType = MarkerType.Circle;
                    linea.MarkerStroke = OxyColors.AntiqueWhite;
                }
                model.Series.Add(linea);
                Grafica.Model = model;
            }
            catch (Exception)
            {
                MessageBox.Show("Algo salio mal", "Congruencial Lineal", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {

            if (Manual.IsChecked == true)
            {

                // Exportar exportar = new Exportar();
                //genera los numeros ps
                for (int i = 0; i < int.Parse(txtMuneroManual.Text); i++)
                {
                    x = ((a * x) + int.Parse(txtCantidad.Text)) % m;

                    //residou 
                    r = x / (m - 1);
                    list1.Items.Add(x);
                    list2.Items.Add(r);
                    TablaCongruencialLineal tabla = new TablaCongruencialLineal();
                    tabla.Posicion = i + 1;
                    tabla.M = int.Parse(x.ToString());
                    tabla.U = (r.ToString());
                    tablaCon.Add(tabla);

                }

                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Title = "Selecciona donde guardar tu archivo";
                saveFileDialog.Filter = "csv files (*.csv)|*.csv";
                if (saveFileDialog.ShowDialog().Value && saveFileDialog.FileName.Length > 0)
                {
                    exportar = new Exportar(saveFileDialog.FileName);
                    if (exportar.Crear())
                    {
                        if (!exportar.EscribirLinea("Xn, M, U"))
                        {
                            MessageBox.Show(exportar.Error, "Error de Creacion csv", MessageBoxButton.OK, MessageBoxImage.Error); //Error al crear linea de encabezado(Archivo incompleto)
                            return;
                        }
                        foreach (var item in tablaCon)
                        {
                            if (!exportar.EscribirLinea(item.ToString()))
                            {
                                MessageBox.Show(exportar.Error, "Error de Creacion csv", MessageBoxButton.OK, MessageBoxImage.Error); //Error al crear linea (Archivo incompleto)
                                return;
                            }
                        }
                        exportar.Cerrar();
                        if (MessageBox.Show($"Proceso de exportacion finalizado, se guardo{exportar.FileName} ¿Desea abrir el archivo?", "Exportacion Finalizada", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                        {
                            if (!exportar.AbrirArchivo())
                                MessageBox.Show(exportar.Error, "Error de Creacion csv", MessageBoxButton.OK, MessageBoxImage.Error); //Error al abrir archivo
                        }
                    }
                    else
                    {
                        MessageBox.Show(exportar.Error, "Error de Creacion csv", MessageBoxButton.OK, MessageBoxImage.Error); //Error al crear archivo
                    }
                }


            }
            else
            {
                // Exportar exportar = new Exportar();
                //genera los numeros ps
                for (int i = 0; i < m; i++)
                {
                    x = ((a * x) + int.Parse(txtCantidad.Text)) % m;

                    //residou 
                    r = x / (m - 1);
                    list1.Items.Add(x);
                    list2.Items.Add(r);
                    TablaCongruencialLineal tabla = new TablaCongruencialLineal();
                    tabla.Posicion = i + 1;
                    tabla.M = int.Parse(x.ToString());
                    tabla.U = (r.ToString());
                    tablaCon.Add(tabla);

                }

                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Title = "Selecciona donde guardar tu archivo";
                saveFileDialog.Filter = "csv files (*.csv)|*.csv";
                if (saveFileDialog.ShowDialog().Value && saveFileDialog.FileName.Length > 0)
                {
                    exportar = new Exportar(saveFileDialog.FileName);
                    if (exportar.Crear())
                    {
                        if (!exportar.EscribirLinea("Xn, M, U"))
                        {
                            MessageBox.Show(exportar.Error, "Error de Creacion csv", MessageBoxButton.OK, MessageBoxImage.Error); //Error al crear linea de encabezado(Archivo incompleto)
                            return;
                        }
                        foreach (var item in tablaCon)
                        {
                            if (!exportar.EscribirLinea(item.ToString()))
                            {
                                MessageBox.Show(exportar.Error, "Error de Creacion csv", MessageBoxButton.OK, MessageBoxImage.Error); //Error al crear linea (Archivo incompleto)
                                return;
                            }
                        }
                        exportar.Cerrar();
                        if (MessageBox.Show($"Proceso de exportacion finalizado, se guardo{exportar.FileName} ¿Desea abrir el archivo?", "Exportacion Finalizada", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                        {
                            if (!exportar.AbrirArchivo())
                                MessageBox.Show(exportar.Error, "Error de Creacion csv", MessageBoxButton.OK, MessageBoxImage.Error); //Error al abrir archivo
                        }
                    }
                    else
                    {
                        MessageBox.Show(exportar.Error, "Error de Creacion csv", MessageBoxButton.OK, MessageBoxImage.Error); //Error al crear archivo
                    }
                }


            }

        }




        private void BtnCalcular_Click(object sender, RoutedEventArgs e)
        {




            ////esto es tuyo
            ///
            if (Manual.IsChecked == true)
            {
                if (VerificarSiEsNumero(txtXo.Text) == true || VerificarSiEsNumero(txtxi.Text) == true || VerificarSiEsNumero(txtxiMod.Text) == true || VerificarSiEsNumero(txtMuneroManual.Text) == true)
                {
                    MessageBox.Show("Los campos deben ser numericos y enteros ", "Erorr", MessageBoxButton.OK, MessageBoxImage.Error);

                }

                if (string.IsNullOrEmpty(txtMuneroManual.Text) || string.IsNullOrEmpty(txtXo.Text) || string.IsNullOrEmpty(txtxi.Text) || string.IsNullOrEmpty(txtxiMod.Text))
                {
                    MessageBox.Show("Datos Incompletos", "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                }
                else
                {


                    tablaCon = new List<TablaCongruencialLineal>();
                    x = double.Parse(txtXo.Text);
                    k = int.Parse(txtxi.Text);
                    g = int.Parse(txtxiMod.Text);
                    //calcular A
                    a = 5 + (8 * k);
                    //calcular el modo 2g
                    //Proceso cs = new Proceso();
                    double b = 2;
                    double exp = double.Parse(txtxiMod.Text);

                    double res = Math.Pow(b, exp);
                    txtM.Text = res.ToString();
                    m = int.Parse(txtM.Text);

                    for (int ii = 0; ii < int.Parse(txtMuneroManual.Text); ii++)
                    {


                        // determinar los numeros primos
                        for (int i = 2; i <= res; i++)
                        {
                            for (int j = 1; j <= int.Parse(txtMuneroManual.Text); j++)
                            {
                                if (i % j == 0) { cont++; }
                            }
                            if (cont == 2) { txtCantidad.Text = i.ToString(); }
                            cont = 0;
                            txt.Text = a.ToString();
                        }
                    }

                }
            }
            else
            {

                if (VerificarSiEsNumero(txtXo.Text) == true || VerificarSiEsNumero(txtxi.Text) == true || VerificarSiEsNumero(txtxiMod.Text) == true)
                {
                    MessageBox.Show("Los campos deben ser numericos y enteros ", "Erorr", MessageBoxButton.OK, MessageBoxImage.Error);

                }


                if (string.IsNullOrEmpty(txtXo.Text) || string.IsNullOrEmpty(txtxi.Text) || string.IsNullOrEmpty(txtxiMod.Text))
                {
                    MessageBox.Show("Datos Incompletos", "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                }
                else
                {

                    tablaCon = new List<TablaCongruencialLineal>();
                    x = double.Parse(txtXo.Text);
                    k = int.Parse(txtxi.Text);
                    g = int.Parse(txtxiMod.Text);
                    //calcular A

                    a = 5 + (8 * k);
                    //calcular el modo 2g
                    //Proceso cs = new Proceso();
                    double b = 2;
                    double exp = double.Parse(txtxiMod.Text);

                    double res = Math.Pow(b, exp);
                    txtM.Text = res.ToString();
                    m = int.Parse(txtM.Text);

                    // determinar los numeros primos
                    for (int i = 2; i <= res; i++)
                    {
                        for (int j = 1; j <= i; j++)
                        {
                            if (i % j == 0) { cont++; }
                        }
                        if (cont == 2) { txtCantidad.Text = i.ToString(); }
                        cont = 0;
                        txt.Text = a.ToString();
                    }
                }

            }
        }

        private void BtnNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCajas();
        }



        private void Automatico_Checked(object sender, RoutedEventArgs e)
        {

            LimpiarCajas();
            PonerEnEdicionAutomatico(true);

        }

        private void Manual_Checked(object sender, RoutedEventArgs e)
        {
            LimpiarCajas();
            PonerEnEdicionManual(true);


            /* for (int i = 2; i <= num; i++)
             {

                 for (int j = 1; j <= i; j++)
                 {
                     if (i % j == 0) { cont++; }
                 }
                 if (cont == 2) { txtMuneroManual.Text = i.ToString(); }
                 cont = 0;
                 txtMuneroManual.Text = a.ToString();
             }
             */

        }

        private void BtnAcercaDe_Click(object sender, RoutedEventArgs e)
        {
            AcercaDeCongruencialMultiplicativo menu = new AcercaDeCongruencialMultiplicativo();
            menu.Show();
            this.Close();
        }

        #endregion

        #region Botones Multiplicacion Constante
        private void BtnNuevoCuadrado_Click(object sender, RoutedEventArgs e)
        {
            txtA.Clear();
            txtNum.Clear();
            txtX0.Clear();
            metodo = new MetodoMultiplicacionConstante();
            txtPosicion.Clear();
            txtValorU.Clear();
            btnCalcular.IsEnabled = true;
            dtgTabla.ItemsSource = null;
        }

        private void BtnCalcularCuadrado_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtA.Text.Length % 2 == 0 && txtX0.Text.Length % 2 == 0)
                {
                    if (radAutomatico.IsChecked == false && radManual.IsChecked == false)
                    {
                        MessageBox.Show("Debe de seleccionar si lo quiere usar automatico o manual", "Multiplicacion Constante", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    metodo.N = radAutomatico.IsChecked == true ? 1000 : int.Parse(txtNum.Text);
                    metodo.x0 = int.Parse(txtX0.Text);
                    metodo.A = int.Parse(txtA.Text);
                    metodo.AlgoritmoMultiplicacionConstante();
                    txtPosicion.Text = metodo.j.ToString();
                    txtValorU.Text = metodo.tabla.Last().U.ToString();// metodo.tabla.Last().Where(p=>p.).ToString();
                    dtgTabla.ItemsSource = metodo.tabla;
                    btnCalcular.IsEnabled = false;
                }
                else
                    MessageBox.Show("Verifica que tus datos sean pares (x0, A) y numeros enteros", "Multiplicacion Constante", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            catch (Exception)
            {
                MessageBox.Show("Verifica que tus datos sean correctos", "Multiplicacion Constanten", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BtnGraficarCuadrado_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LinearAxis x = new LinearAxis()
                {
                    Minimum = double.Parse(metodo.tabla.Min(p => p.Posicion).ToString()),
                    Maximum = double.Parse(metodo.tabla.Max(p => p.Posicion).ToString()),
                    Position = AxisPosition.Bottom,
                };
                LinearAxis y = new LinearAxis()
                {

                    Minimum = double.Parse(metodo.tabla.Min(p => p.U)),
                    Maximum = double.Parse(metodo.tabla.Max(p => p.U)),
                    Position = AxisPosition.Left,
                };

                PlotModel model = new PlotModel();
                model.Axes.Add(x);
                model.Axes.Add(y);
                model.Title = "Grafica";


                LineSeries linea = new LineSeries();

                //  linea.Color = OxyColor.FromRgb(byte.Parse(r.Next(0, 255).ToString()), byte.Parse(r.Next(0, 255).ToString()), byte.Parse(r.Next(0, 255).ToString()));
                // linea.LineStyle = LineStyle.None;
                //   model.Series.Add(linea);
                foreach (var item in metodo.tabla)
                {

                    linea.Points.Add(new DataPoint(item.Posicion, double.Parse(item.U)));
                    linea.LineStyle = LineStyle.None;
                    linea.MarkerType = MarkerType.Circle;
                    linea.MarkerStroke = OxyColors.AntiqueWhite;



                }
                model.Series.Add(linea);
                Grafica.Model = model;
            }
            catch (Exception)
            {
                MessageBox.Show("Algo salio mal", "Multiplicacion Constante", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BtnExportarCuadrado_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Title = "Selecciona donde guardar tu archivo";
            saveFileDialog.Filter = "csv files (*.csv)|*.csv";
            if (saveFileDialog.ShowDialog().Value && saveFileDialog.FileName.Length > 0)
            {
                exportar = new Exportar(saveFileDialog.FileName);
                if (exportar.Crear())
                {
                    if (!exportar.EscribirLinea("Xn, A, Producto, U"))
                    {
                        MessageBox.Show(exportar.Error, "Error de Creacion csv", MessageBoxButton.OK, MessageBoxImage.Error); //Error al crear linea de encabezado(Archivo incompleto)
                        return;
                    }
                    foreach (var item in metodo.tabla)
                    {
                        if (!exportar.EscribirLinea(item.ToString()))
                        {
                            MessageBox.Show(exportar.Error, "Error de Creacion csv", MessageBoxButton.OK, MessageBoxImage.Error); //Error al crear linea (Archivo incompleto)
                            return;
                        }
                    }
                    exportar.Cerrar();
                    if (MessageBox.Show($"Proceso de exportacion finalizado, se guardo{exportar.FileName} ¿Desea abrir el archivo?", "Exportacion Finalizada", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        if (!exportar.AbrirArchivo())
                            MessageBox.Show(exportar.Error, "Error de Creacion csv", MessageBoxButton.OK, MessageBoxImage.Error); //Error al abrir archivo
                    }
                }
                else
                {
                    MessageBox.Show(exportar.Error, "Error de Creacion csv", MessageBoxButton.OK, MessageBoxImage.Error); //Error al crear archivo
                }
            }
        }
        #endregion
    }
}
