﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PrimeraUnidad.Metodos
{
    /// <summary>
    /// Lógica de interacción para cuadrado.xaml
    /// </summary>
    public partial class cuadrado : Window
    {
        CuadradoMedios medios;
        public cuadrado()
        {
            InitializeComponent();
            medios = new CuadradoMedios();
            medios.N = 500;
            medios.Semilla = 259792;
            medios.MetodoCuadradosMedios();
        }
    }
}
