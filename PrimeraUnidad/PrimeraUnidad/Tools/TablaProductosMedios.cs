﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeraUnidad.Tools
{
    public class TablaProductosMedios
    {
        public float x { get; set; }
        public double xCuadrada { get; set; }
        public string _x { get; set; }
        public string u { get; set; }
        public override string ToString()
        {
            return $"{x}, {xCuadrada}, {_x},  {u}";
        }
    }
}
