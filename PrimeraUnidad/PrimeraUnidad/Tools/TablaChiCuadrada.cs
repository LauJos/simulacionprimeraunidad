﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeraUnidad.Tools
{
    public class TablaChiCuadrada
    {
        public double Intervalos { get; set; }
        public int Total { get; set; }//numero de datos que se repiten
        public double Resta { get; set; }
        public double Cuadrado { get; set; }
        public double Decimal { get; set; }
        public override string ToString()
        {
            return $"{Intervalos} => {Total}";
        }
    }
}
