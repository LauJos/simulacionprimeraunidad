﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeraUnidad.Tools
{
    public class TablaCongruencialLineal
    {
        public int Posicion { get; set; }
        public int M { get; set; }
        //public int A { get; set; }
        //public string Producto { get; set; }
        public string U { get; set; }

        public override string ToString()
        {
            return $"{Posicion},    {M},  {U}";
        }
    }
}
