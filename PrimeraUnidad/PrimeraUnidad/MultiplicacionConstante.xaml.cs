﻿using Microsoft.Win32;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using PrimeraUnidad.Metodos;
using PrimeraUnidad.Tools;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PrimeraUnidad
{
    /// <summary>
    /// Lógica de interacción para MultiplicacionConstante.xaml
    /// </summary>
    public partial class MultiplicacionConstante : Window
    {
        MetodoMultiplicacionConstante metodo;
        Exportar exportar;
        Random r = new Random();
        public MultiplicacionConstante()
        {
            InitializeComponent();
            metodo = new MetodoMultiplicacionConstante();
        }

        private void BtnCalcular_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtA.Text.Length % 2 == 0 && txtX0.Text.Length % 2 == 0)
                {
                    if (radAutomatico.IsChecked == false && radManual.IsChecked == false)
                    {
                        MessageBox.Show("Debe de seleccionar si lo quiere usar automatico o manual", "Multiplicacion Constante", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    } metodo.N = radAutomatico.IsChecked == true ? 1000 : int.Parse(txtNum.Text);
                    metodo.x0 = int.Parse(txtX0.Text);
                    metodo.A = int.Parse(txtA.Text);
                    metodo.AlgoritmoMultiplicacionConstante();
                    txtPosicion.Text = metodo.j.ToString();
                    txtValorU.Text = metodo.tabla.Last().U.ToString();// metodo.tabla.Last().Where(p=>p.).ToString();
                    dtgTablaMultiplicacionConstante.ItemsSource = metodo.tabla;
                    btnCalcular.IsEnabled = false;
                } else
                    MessageBox.Show("Verifica que tus datos sean pares (x0, A) y numeros enteros", "Multiplicacion Constante", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            catch (Exception)
            {
                MessageBox.Show("Verifica que tus datos sean correctos", "Multiplicacion Constanten", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        

        private void BtnEjemplo_Click(object sender, RoutedEventArgs e)
        {
           // string cadena = "%windir%\AlgoritmoMultiplicacionConstante.pdf/select";
            //System.Diagnostics.Process.Start(cadena);
        }

        

        private void BtnNuevo_Click(object sender, RoutedEventArgs e)
        {
            txtA.Clear();
            txtNum.Clear();
            txtX0.Clear();
            metodo = new MetodoMultiplicacionConstante();
            txtPosicion.Clear();
            txtValorU.Clear();
            btnCalcular.IsEnabled = true;
            dtgTablaMultiplicacionConstante.ItemsSource = null;
        }

        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Title = "Selecciona donde guardar tu archivo";
            saveFileDialog.Filter = "csv files (*.csv)|*.csv";
            if (saveFileDialog.ShowDialog().Value && saveFileDialog.FileName.Length > 0)
            {
                exportar = new Exportar(saveFileDialog.FileName);
                if (exportar.Crear())
                {
                    if (!exportar.EscribirLinea("Xn, A, Producto, U"))
                    {
                        MessageBox.Show(exportar.Error, "Error de Creacion csv", MessageBoxButton.OK, MessageBoxImage.Error); //Error al crear linea de encabezado(Archivo incompleto)
                        return;
                    }
                    foreach (var item in metodo.tabla)
                    {
                        if (!exportar.EscribirLinea(item.ToString()))
                        {
                            MessageBox.Show(exportar.Error, "Error de Creacion csv", MessageBoxButton.OK, MessageBoxImage.Error); //Error al crear linea (Archivo incompleto)
                            return;
                        }
                    }
                    exportar.Cerrar();
                    if (MessageBox.Show($"Proceso de exportacion finalizado, se guardo{exportar.FileName} ¿Desea abrir el archivo?", "Exportacion Finalizada", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        if (!exportar.AbrirArchivo())
                            MessageBox.Show(exportar.Error, "Error de Creacion csv", MessageBoxButton.OK, MessageBoxImage.Error); //Error al abrir archivo
                    }
                }
                else
                {
                    MessageBox.Show(exportar.Error, "Error de Creacion csv", MessageBoxButton.OK, MessageBoxImage.Error); //Error al crear archivo
                }
            }
        }

        private void BtnGraficar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LinearAxis x = new LinearAxis()
                {
                    Minimum = double.Parse(metodo.tabla.Min(p => p.Posicion).ToString()),
                    Maximum = double.Parse(metodo.tabla.Max(p => p.Posicion).ToString()),
                    Position = AxisPosition.Bottom,
                };
                LinearAxis y = new LinearAxis()
                {

                    Minimum = double.Parse(metodo.tabla.Min(p => p.U)),
                    Maximum = double.Parse(metodo.tabla.Max(p => p.U)),
                    Position = AxisPosition.Left,
                };

                PlotModel model = new PlotModel();
                model.Axes.Add(x);
                model.Axes.Add(y);
                model.Title = "Grafica";
                

                LineSeries linea = new LineSeries();
                
              //  linea.Color = OxyColor.FromRgb(byte.Parse(r.Next(0, 255).ToString()), byte.Parse(r.Next(0, 255).ToString()), byte.Parse(r.Next(0, 255).ToString()));
               // linea.LineStyle = LineStyle.None;
             //   model.Series.Add(linea);
                foreach (var item in metodo.tabla)
                {

                    linea.Points.Add(new DataPoint(item.Posicion,double.Parse(item.U)));
                    linea.LineStyle = LineStyle.None;
                    linea.MarkerType = MarkerType.Circle;
                    linea.MarkerStroke = OxyColors.AntiqueWhite;

                        

                }
                model.Series.Add(linea);
                Grafica.Model = model;
            }
            catch (Exception)
            {
                MessageBox.Show("Algo salio mal", "Multiplicacion Constante", MessageBoxButton.OK, MessageBoxImage.Error);                
            }
        }

        private void BtnCerrar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnVolver_Click(object sender, RoutedEventArgs e)
        {
            Menu menu = new Menu();
            menu.Show();
            this.Close();
        }
    }
}
