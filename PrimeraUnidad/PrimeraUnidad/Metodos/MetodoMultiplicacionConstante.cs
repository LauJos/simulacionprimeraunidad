﻿using PrimeraUnidad.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeraUnidad.Metodos
{
    public class MetodoMultiplicacionConstante
    {
        public int x0 { get; set; }//afuera
        public int A { get; set; }
        public double Producto { get; set; }
        public int _Longitud { get; set; }
        public string X { get; set; }//numero al cuadrado
        public int N { get; set; }//Resultado
        public int Longitud { get; set; }
        public int j { get; set; }//Cantidad ciclo
        public List<TablaMultiplicacionConstante> tabla { get; set; }
        public void AlgoritmoMultiplicacionConstante()
        {
            _Longitud = x0.ToString().Length;
            tabla = new List<TablaMultiplicacionConstante>();          
           
            j = 0;
            int h = 0;

            do
            {
                j++;
                Longitud = x0.ToString().Length;
                Producto = x0*A;
                TablaMultiplicacionConstante entidad = new TablaMultiplicacionConstante();
                entidad.X = x0; //datos para la exportacion posicion
                entidad.A = A; //datos para la exportacion numero constante

                if (_Longitud != (Producto.ToString().Length / 2))
                {
                    h = (_Longitud * 2) - Producto.ToString().Length;
                    for (int t = 1; t <= h; t++)
                    {
                        X = "0" + X;
                    }
                    X = X + Producto.ToString();
                }
                else
                    X = Producto.ToString();

                entidad.Producto = X;//datos para la exportacion del producto

                //para comparar el tamaño 
                if (Longitud < _Longitud)
                    X = "0." + X.Substring((_Longitud / 2), _Longitud);
                else
                    X = "0." + X.Substring((Longitud / 2), Longitud);

                entidad.U = X;//datos para la exportacion
                entidad.Posicion = j;
                tabla.Add(entidad);//datos para la exportacion
                //Comprobar si no existe un numero igual en esa lista
                if ((tabla.Where(p => p.U == X).Count()) >= 2)
                    break;
                //Actualizar Datos
                x0 = int.Parse(X.Substring(2, X.Length - 2));             

                X = "";

            } while (j <= N);

        }
       
    }
}
