﻿using Meta.Numerics.Statistics.Distributions;
using PrimeraUnidad.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeraUnidad.Metodos
{
    public class AlgoritmoChiCuadrada
    {
        public int Confianza { get; set; }//nivel de confianza
        public double M { get; set; }//raiz de n
        public int N { get; set; }//total de datos
        public List<double> Numeros { get; set; }
        public List<TablaChiCuadrada> tabla { get; set; }
        public double Chi { get; set; }
        public double NivelAceptacion { get; set; }
        public void Solucion() {
            tabla = new List<TablaChiCuadrada>();
            Double intervalo = 1 / M;
            int i = 0;

            double inicio=0, fin=intervalo;

            while (i < M) {
                int contador = 0;
                foreach (var item in Numeros)
                {
                    if (item > inicio  && item <= fin)
                        contador++;
                }
                tabla.Add(new TablaChiCuadrada()
                {
                    Intervalos = fin,
                    Total = contador,
                    Resta = M - contador,
                    Cuadrado = Math.Pow(M - contador, 2),
                    Decimal = Math.Pow(M - contador, 2) * 0.10                    
                }) ;
                Chi = Chi + Math.Pow(M - contador, 2) * 0.10;
                inicio = fin;
                fin = fin + intervalo;
                i++;

            }

        }

        internal string CalcularDistribucion()
        {
            double alpha = (NivelAceptacion / 100);
            ChiSquaredDistribution chiCuadrada = new ChiSquaredDistribution(M-1);
            double chi = chiCuadrada.InverseRightProbability(alpha);
            if (Chi < chi)
                return "No se puede rechazar el conjunto de U, sigue una distribucion uniforme (" + Chi + "<" + chi + ")";
            else
                return "Se rechaza que U sigue una distribucion uniforme (" + Chi + "<" + chi + ")";
        }
    }
}
