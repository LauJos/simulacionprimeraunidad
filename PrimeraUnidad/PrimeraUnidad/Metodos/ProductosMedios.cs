﻿using PrimeraUnidad.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeraUnidad.Metodos
{
    public class ProductosMedios
    {
        public int X0 { get; set; }
        public int X1 { get; set; }
        public int X2 { get; set; }
        public float Producto { get; set; }
        public int U1 { get; set; }
        public int N { get; set; }//numero que especifique el usuario
        public string Aleatorio { get; set; }
        public List<string> Aleatorios { get; set; }
  
        public void AlgoritmoProductosMedios()
        {
            int j = 0, h=0;
            int _Longitud =X1.ToString().Length;
            int Longitud = 0;
            do

            {
                Producto = X0 * X1;
                Longitud = Producto.ToString().Length;
                if (_Longitud * 2 != Longitud)
                {
                    h = (_Longitud * 2) - Longitud;
                    for (int t = 1; t <= h; t++)
                    {
                        Aleatorio = "0" + Aleatorio;
                    }
                    Aleatorio = Aleatorio + Producto.ToString();
                }
                else
                    Aleatorio = Producto.ToString();

                if (Longitud < _Longitud)
                    Aleatorio = "0." + Aleatorio.Substring((_Longitud / 2), _Longitud);
                else
                    Aleatorio = "0." + Aleatorio.Substring((Longitud / 2), Longitud);

                Aleatorios.Add(Aleatorio);

                if ((Aleatorios.Where(p => p.ToString() == Aleatorio).Count()) >= 2)
                    break;
                j++;
            } while (j<N);
        }

    }
}
