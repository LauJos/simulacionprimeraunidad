﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeraUnidad.Archivos
{
    public class AccionesArchivo
    {
        public string Archivo { get; set; }
        public AccionesArchivo(string archivo) {
            Archivo = archivo;
        }

        public string Lectura() {
            try
            {
                StreamReader fila = new StreamReader(Archivo);
                string elementos = fila.ReadToEnd();
                fila.Close();
                return elementos;
            }
            catch (Exception)
            {
                return null;
            }
        }

       
    }
}
