﻿using System.Data.OleDb;
using Microsoft.Win32;
using PrimeraUnidad.Archivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using PrimeraUnidad.Metodos;

namespace PrimeraUnidad
{
    /// <summary>
    /// Lógica de interacción para ChiCuadrada.xaml
    /// </summary>
    public partial class ChiCuadrada : Window
    {
        AccionesArchivo acciones;
        AlgoritmoChiCuadrada algoritmo;
        List<double> numeros;
        public ChiCuadrada()
        {
            InitializeComponent();
            algoritmo = new AlgoritmoChiCuadrada();
        }

        private void BtnAbrirArchivo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog openDialog = new OpenFileDialog();
                openDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                openDialog.DefaultExt = "*.log";
                openDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                if (openDialog.ShowDialog() == true)
                {
                    acciones = new AccionesArchivo(openDialog.FileName);
                    string elementos = acciones.Lectura();
                    if (elementos != null)
                    {
                        numeros = new List<double>();
                        string[] fila = elementos.Split('\n');

                        for (int i = 0; i < fila.Length; i++)
                        {
                            string[] espacio = fila[i].Split(' ');
                            numeros.Add(double.Parse(espacio[0].ToString()));
                        }
                    }

                }
                MessageBox.Show("Archivo extraido correctamente", "Chi-Cuadrada", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }          

        }

        private void BtnPrueba_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                algoritmo.N = numeros.Count();
                algoritmo.M = Math.Sqrt(algoritmo.N);
                algoritmo.Numeros = numeros;
                algoritmo.NivelAceptacion = double.Parse(txtNivelConfianza.Text);
                algoritmo.Solucion();
                dtgTabla.ItemsSource = algoritmo.tabla;
                txtX.Text = algoritmo.Chi.ToString();
                txtConclusion.Text = algoritmo.CalcularDistribucion();
            }
            catch (Exception)
            {
                MessageBox.Show("Error!! Llena todos los campos", "ChiCuadrada", MessageBoxButton.OK, MessageBoxImage.Error);
            }           
        }        
    }
}

